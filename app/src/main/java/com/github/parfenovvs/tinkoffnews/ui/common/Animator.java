package com.github.parfenovvs.tinkoffnews.ui.common;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.view.View;

import com.github.parfenovvs.tinkoffnews.R;

public class Animator {
	public static void showContent(final View loadingView, final View contentView, final View errorView) {
		errorView.setVisibility(View.GONE);
		if (contentView.getVisibility() == View.VISIBLE) {
			loadingView.setVisibility(View.GONE);
		} else {
			Resources resources = loadingView.getResources();
			AnimatorSet set = new AnimatorSet();

			ObjectAnimator contentFadeIn = ObjectAnimator.ofFloat(contentView, View.ALPHA, 0f, 1f);
			ObjectAnimator loadingFadeOut = ObjectAnimator.ofFloat(loadingView, View.ALPHA, 1f, 0f);

			set.playTogether(contentFadeIn, loadingFadeOut);
			set.setDuration(resources.getInteger(R.integer.lce_anim_duration));

			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(android.animation.Animator animation) {
					loadingView.setVisibility(View.GONE);
					loadingView.setAlpha(1f);
				}

				@Override
				public void onAnimationStart(android.animation.Animator animation) {
					contentView.setVisibility(View.VISIBLE);
				}
			});

			set.start();
		}
	}
}