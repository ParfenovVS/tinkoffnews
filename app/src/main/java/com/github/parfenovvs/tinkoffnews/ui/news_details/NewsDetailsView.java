package com.github.parfenovvs.tinkoffnews.ui.news_details;

import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

interface NewsDetailsView extends MvpLceView<NewsDetails> {
}
