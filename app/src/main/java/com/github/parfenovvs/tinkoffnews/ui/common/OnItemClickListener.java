package com.github.parfenovvs.tinkoffnews.ui.common;

public interface OnItemClickListener {
	void onClick(int position);
}
