package com.github.parfenovvs.tinkoffnews.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class NewsItem extends RealmObject {
	@PrimaryKey
	private String id;
	private String text;
	private long publicationDate;

	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public long getPublicationDate() {
		return publicationDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setPublicationDate(long publicationDate) {
		this.publicationDate = publicationDate;
	}
}
