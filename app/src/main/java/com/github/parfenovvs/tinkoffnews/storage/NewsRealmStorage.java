package com.github.parfenovvs.tinkoffnews.storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.parfenovvs.tinkoffnews.common.Func1;
import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;

import java.util.ArrayList;
import java.util.List;

public class NewsRealmStorage implements NewsStorage {
	private static final String FILE_NAME = "news";
	private static final String KEY_ID = "id";

	@NonNull
	@Override
	public List<NewsItem> getNews() {
		BaseRealmStorage storage = createRealm();
		List<NewsItem> news = storage.getRealm()
				.where(NewsItem.class)
				.findAll();
		if (news.isEmpty()) {
			return new ArrayList<>();
		}
		news = storage.getRealm().copyFromRealm(news);
		storage.close();
		return news;
	}

	@Nullable
	@Override
	public NewsDetails getNewsDetails(String id) {
		BaseRealmStorage storage = createRealm();
		NewsDetails detailsEntity = storage.getRealm()
				.where(NewsDetails.class)
				.equalTo(KEY_ID, id)
				.findFirst();
		if (detailsEntity == null) {
			return null;
		}
		detailsEntity = storage.getRealm().copyFromRealm(detailsEntity);
		storage.close();
		return detailsEntity;
	}

	@Override
	public void transaction(Func1<NewsTransaction> transactionBody) {
		createRealm().transaction(transactionBody);
	}

	private BaseRealmStorage<NewsTransaction> createRealm() {
		return new BaseRealmStorage<>(FILE_NAME, NewsRealmTransaction::new);
	}
}
