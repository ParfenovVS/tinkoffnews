package com.github.parfenovvs.tinkoffnews.common;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateFormatUtil {
	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("HH:mm, dd MMMM yyyy");

	public static String format(long instant) {
		return FORMATTER.print(instant);
	}
}
