package com.github.parfenovvs.tinkoffnews.ui.news_list;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.parfenovvs.tinkoffnews.R;
import com.github.parfenovvs.tinkoffnews.ui.common.OnItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsVH> {
	private List<NewsViewModel> items;
	private OnItemClickListener onItemClickListener;

	@Override
	public NewsVH onCreateViewHolder(ViewGroup parent, int viewType) {
		return new NewsVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false), onItemClickListener);
	}

	@Override
	public void onBindViewHolder(NewsVH holder, int position) {
		holder.bind(items.get(position));
	}

	@Override
	public int getItemCount() {
		return items == null ? 0 : items.size();
	}

	void setItems(List<NewsViewModel> items) {
		this.items = items;
	}

	@Nullable
	List<NewsViewModel> getItems() {
		return items;
	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	static class NewsVH extends RecyclerView.ViewHolder {
		@BindView(R.id.text) TextView textView;
		@BindView(R.id.date) TextView dateTextView;

		NewsVH(View itemView, OnItemClickListener onItemClickListener) {
			super(itemView);
			ButterKnife.bind(this, itemView);

			itemView.setOnClickListener(v -> {
				if (onItemClickListener != null) {
					onItemClickListener.onClick(getAdapterPosition());
				}
			});
		}

		void bind(NewsViewModel item) {
			textView.setText(item.getText());
			dateTextView.setText(item.getDate());
		}
	}
}
