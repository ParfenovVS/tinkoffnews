package com.github.parfenovvs.tinkoffnews.di;

import android.support.annotation.NonNull;

import com.github.parfenovvs.tinkoffnews.api.DefaultNewsServiceFactory;
import com.github.parfenovvs.tinkoffnews.api.NewsService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApiModule {
	@Provides
	@NonNull
	@Singleton
	public NewsService provideNewsService() {
		return new DefaultNewsServiceFactory().createNewsService();
	}
}
