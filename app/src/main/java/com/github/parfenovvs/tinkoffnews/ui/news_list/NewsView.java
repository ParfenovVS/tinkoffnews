package com.github.parfenovvs.tinkoffnews.ui.news_list;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

interface NewsView extends MvpLceView<List<NewsViewModel>> {
	void goToNewsDetails(String id);
}
