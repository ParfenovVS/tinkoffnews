package com.github.parfenovvs.tinkoffnews.common;

import android.os.Build;
import android.text.Html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class ParseUtil {
	public static String stripHtml(String html) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
			return Html.fromHtml(html).toString();
		}
		return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
	}

	public static String exportContent(String html) {
		Element doc = Jsoup.parse(html);
		return stripHtml(doc.text());
	}
}
