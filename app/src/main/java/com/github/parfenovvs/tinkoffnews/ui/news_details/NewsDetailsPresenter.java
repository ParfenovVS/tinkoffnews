package com.github.parfenovvs.tinkoffnews.ui.news_details;

import com.github.parfenovvs.tinkoffnews.NewsApplication;
import com.github.parfenovvs.tinkoffnews.common.DateFormatUtil;
import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.repository.NewsRepository;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NewsDetailsPresenter extends MvpBasePresenter<NewsDetailsView> {
	private final String newsId;
	private NewsDetails newsDetails;
	@Inject NewsRepository repository;

	NewsDetailsPresenter(String newsId) {
		NewsApplication.appComponent().inject(this);
		this.newsId = newsId;
	}

	public NewsDetails getData() {
		return newsDetails;
	}

	void loadData() {
		getView().showLoading(false);

		repository.newsDetails(newsId, false)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(details -> {
					this.newsDetails = details;
					getView().setData(details);
					getView().showContent();
				}, throwable -> getView().showError(throwable, false));
	}

	String formatDate(long instant) {
		return DateFormatUtil.format(instant);
	}

}
