package com.github.parfenovvs.tinkoffnews.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.parfenovvs.tinkoffnews.NewsApplication;
import com.github.parfenovvs.tinkoffnews.api.BadResponseException;
import com.github.parfenovvs.tinkoffnews.api.NewsService;
import com.github.parfenovvs.tinkoffnews.api.entity.NewsDetailsEntity;
import com.github.parfenovvs.tinkoffnews.api.entity.NewsEntity;
import com.github.parfenovvs.tinkoffnews.api.entity.ResponseEntity;
import com.github.parfenovvs.tinkoffnews.common.ParseUtil;
import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;
import com.github.parfenovvs.tinkoffnews.storage.NewsStorage;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.Lazy;
import io.reactivex.Observable;

public class DefaultNewsRepository implements NewsRepository {
	@Inject Lazy<NewsStorage> newsStorage;
	@Inject NewsService newsService;

	public DefaultNewsRepository() {
		NewsApplication.appComponent().inject(this);
	}

	@Override
	public Observable<List<NewsItem>> news(boolean invalidate) {
		return Observable.defer(() -> {
			if (!invalidate) {
				List<NewsItem> newsEntities = newsStorage.get().getNews();
				if (!newsEntities.isEmpty()) {
					return Observable.just(newsEntities);
				}
			}
			return newsService.news()
					.doOnNext(this::validateResponse)
					.map(response -> mapToModel(response.getPayload()))
					.doOnNext(this::clearHtmlData)
					.doOnNext(this::cache);
		});
	}

	@Override
	public Observable<NewsDetails> newsDetails(String id, boolean invalidate) {
		return Observable.defer(() -> {
			if (!invalidate) {
				NewsDetails detailsEntity = newsStorage.get().getNewsDetails(id);
				if (detailsEntity != null) {
					return Observable.just(detailsEntity);
				}
			}
			return newsService.newsDetails(id)
					.doOnNext(this::validateResponse)
					.map(response -> mapToModel(response.getPayload()))
					.doOnNext(this::clearHtmlData)
					.doOnNext(this::cache);
		});
	}

	private void validateResponse(ResponseEntity response) throws Exception {
		if (response != null && !response.getResultCode().equals(ResponseEntity.OK)) {
			throw new BadResponseException("Result code: " + response.getResultCode());
		}
	}

	private List<NewsItem> mapToModel(@NonNull List<NewsEntity> entities) {
		List<NewsItem> items = new ArrayList<>();

		for (NewsEntity e : entities) {
			NewsItem item = new NewsItem();

			item.setId(e.getId());
			item.setPublicationDate(e.getPublicationDate().getMilliseconds());
			item.setText(e.getText());

			items.add(item);
		}

		return items;
	}

	private NewsDetails mapToModel(@Nullable NewsDetailsEntity entity) {
		if (entity == null) {
			return null;
		}

		NewsDetails details = new NewsDetails();

		details.setId(entity.getTitle().getId());
		details.setText(entity.getTitle().getText());
		details.setPublicationDate(entity.getTitle().getPublicationDate().getMilliseconds());
		details.setContent(entity.getContent());
		details.setCreationDate(entity.getCreationDate().getMilliseconds());
		details.setLastModificationDate(entity.getLastModificationDate().getMilliseconds());

		return details;
	}

	private void cache(List<NewsItem> news) {
		newsStorage.get().transaction(newsTransaction -> {
			newsTransaction.clear();
			newsTransaction.add(news);
		});
	}

	private void cache(NewsDetails news) {
		newsStorage.get().transaction(newsTransaction -> newsTransaction.add(news));
	}

	private void clearHtmlData(List<NewsItem> items) {
		for (NewsItem item : items) {
			item.setText(ParseUtil.stripHtml(item.getText()));
		}
	}

	private void clearHtmlData(NewsDetails details) {
		details.setText(ParseUtil.stripHtml(details.getText()));
		details.setContent(ParseUtil.exportContent(details.getContent()));
	}
}
