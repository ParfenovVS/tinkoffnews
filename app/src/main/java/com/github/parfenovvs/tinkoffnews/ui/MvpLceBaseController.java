package com.github.parfenovvs.tinkoffnews.ui;

import android.os.Bundle;
import android.view.View;

import com.github.parfenovvs.tinkoffnews.ui.common.Animator;
import com.hannesdorfmann.mosby3.conductor.viewstate.lce.MvpLceViewStateController;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public abstract class MvpLceBaseController<CV extends View, M, V extends MvpLceView<M>, P extends MvpBasePresenter<V>> extends MvpLceViewStateController<CV, M, V, P> {

	public MvpLceBaseController(Bundle args) {
		super(args);
	}

	@Override
	protected void animateContentViewIn() {
		Animator.showContent(loadingView, contentView, errorView);
	}
}
