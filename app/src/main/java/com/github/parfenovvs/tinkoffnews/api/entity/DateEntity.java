package com.github.parfenovvs.tinkoffnews.api.entity;

public class DateEntity {
	private long milliseconds;

	public long getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(long milliseconds) {
		this.milliseconds = milliseconds;
	}
}
