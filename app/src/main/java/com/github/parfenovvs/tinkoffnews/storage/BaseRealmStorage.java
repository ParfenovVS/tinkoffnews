package com.github.parfenovvs.tinkoffnews.storage;

import com.github.parfenovvs.tinkoffnews.common.Func1;
import com.github.parfenovvs.tinkoffnews.common.Func1R;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseRealmStorage<T extends RealmTransaction> {
	private Realm realm;
	private Func1R<Realm, T> transactionBuilder;

	public BaseRealmStorage(final String fileName, Func1R<Realm, T> transactionBuilder) {
		realm = Realm.getInstance(new RealmConfiguration.Builder()
				.name(fileName)
				.deleteRealmIfMigrationNeeded()
				.schemaVersion(1)
				.build());
		this.transactionBuilder = transactionBuilder;
	}

	public Realm getRealm() {
		return realm;
	}

	public void transaction(Func1<T> transactionBody) {
		realm.executeTransaction(r -> transactionBody.call(transactionBuilder.call(r)));
		close();
	}

	public void close() {
		if (!realm.isClosed()) {
			realm.close();
		}
	}
}
