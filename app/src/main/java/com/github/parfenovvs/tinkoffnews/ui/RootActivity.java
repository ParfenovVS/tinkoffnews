package com.github.parfenovvs.tinkoffnews.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.github.parfenovvs.tinkoffnews.R;
import com.github.parfenovvs.tinkoffnews.ui.news_list.NewsController;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RootActivity extends AppCompatActivity {
	private Router router;
	@BindView(R.id.container) ViewGroup containerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_root);
		ButterKnife.bind(this);

		router = Conductor.attachRouter(this, containerView, savedInstanceState);
		if (!router.hasRootController()) {
			router.setRoot(RouterTransaction.with(new NewsController(null)));
		}
	}

	@Override
	public void onBackPressed() {
		if (!router.handleBack()) {
			super.onBackPressed();
		}
	}
}
