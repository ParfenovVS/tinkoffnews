package com.github.parfenovvs.tinkoffnews.ui.news_list;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler;
import com.github.parfenovvs.tinkoffnews.R;
import com.github.parfenovvs.tinkoffnews.ui.MvpLceBaseController;
import com.github.parfenovvs.tinkoffnews.ui.news_details.NewsDetailsController;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.RetainingLceViewState;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsController extends MvpLceBaseController<RecyclerView, List<NewsViewModel>, NewsView, NewsPresenter>
		implements NewsView, SwipeRefreshLayout.OnRefreshListener {
	private NewsAdapter adapter;
	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
	@BindView(R.id.contentView) RecyclerView recyclerView;

	public NewsController(Bundle args) {
		super(args);
	}

	@NonNull
	@Override
	protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
		View view = inflater.inflate(R.layout.controller_news, container, false);
		ButterKnife.bind(this, view);

		toolbar.setOnClickListener(v -> recyclerView.smoothScrollToPosition(0));

		swipeRefreshLayout.setOnRefreshListener(this);
		swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
		recyclerView.addItemDecoration(new DividerItemDecoration(container.getContext(), DividerItemDecoration.VERTICAL));
		recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));

		adapter = new NewsAdapter();

		recyclerView.setAdapter(adapter);

		return view;
	}

	@Override
	public List<NewsViewModel> getData() {
		return adapter.getItems();
	}

	@Override
	public void setData(List<NewsViewModel> data) {
		adapter.setItems(data);
		adapter.setOnItemClickListener(presenter::onNewsItemClick);
		adapter.notifyDataSetChanged();
		swipeRefreshLayout.setRefreshing(false);
	}

	@NonNull
	@Override
	public LceViewState<List<NewsViewModel>, NewsView> createViewState() {
		return new RetainingLceViewState<>();
	}

	@Override
	protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
		Resources resources = getResources();
		if (resources != null) {
			if (pullToRefresh) {
				return getResources().getString(R.string.loading_error);
			} else {
				return getResources().getString(R.string.try_again);
			}
		}
		return "";
	}

	@NonNull
	@Override
	public NewsPresenter createPresenter() {
		return new NewsPresenter();
	}

	@Override
	public void loadData(boolean pullToRefresh) {
		presenter.loadNews(pullToRefresh);
	}

	@Override
	public void showError(Throwable e, boolean pullToRefresh) {
		super.showError(e, pullToRefresh);
		swipeRefreshLayout.setRefreshing(false);
	}

	@Override
	public void goToNewsDetails(String id) {
		getRouter().pushController(RouterTransaction
				.with(NewsDetailsController.newInstance(id))
				.pushChangeHandler(new HorizontalChangeHandler())
				.popChangeHandler(new HorizontalChangeHandler()));
	}

	@Override
	public void onRefresh() {
		loadData(true);
	}
}
