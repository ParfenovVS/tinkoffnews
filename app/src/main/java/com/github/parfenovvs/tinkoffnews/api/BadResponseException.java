package com.github.parfenovvs.tinkoffnews.api;

public class BadResponseException extends Exception {
	public BadResponseException(String message) {
		super(message);
	}
}
