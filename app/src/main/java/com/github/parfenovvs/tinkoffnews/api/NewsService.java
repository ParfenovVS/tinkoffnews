package com.github.parfenovvs.tinkoffnews.api;

import com.github.parfenovvs.tinkoffnews.api.entity.NewsDetailsEntity;
import com.github.parfenovvs.tinkoffnews.api.entity.NewsEntity;
import com.github.parfenovvs.tinkoffnews.api.entity.ResponseEntity;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsService {
	@GET("/v1/news")
	Observable<ResponseEntity<List<NewsEntity>>> news();

	@GET("/v1/news_content")
	Observable<ResponseEntity<NewsDetailsEntity>> newsDetails(@Query("id") String newsId);
}
