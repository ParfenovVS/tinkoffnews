package com.github.parfenovvs.tinkoffnews.di;

import android.support.annotation.NonNull;

import com.github.parfenovvs.tinkoffnews.repository.DefaultNewsRepository;
import com.github.parfenovvs.tinkoffnews.repository.NewsRepository;
import com.github.parfenovvs.tinkoffnews.storage.NewsRealmStorage;
import com.github.parfenovvs.tinkoffnews.storage.NewsStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsModule {

	@Provides
	@NonNull
	public NewsStorage provideNewsStorage() {
		return new NewsRealmStorage();
	}

	@Provides
	@NonNull
	@Singleton
	public NewsRepository provideNewsRepository() {
		return new DefaultNewsRepository();
	}
}
