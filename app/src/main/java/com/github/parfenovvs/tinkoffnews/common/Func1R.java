package com.github.parfenovvs.tinkoffnews.common;

public interface Func1R<T, R> {
	R call(T t);
}
