package com.github.parfenovvs.tinkoffnews.api.entity;

public class ResponseEntity<T> {
	public static final String OK = "OK";
	private String resultCode;
	private T payload;

	public String getResultCode() {
		return resultCode;
	}

	public T getPayload() {
		return payload;
	}
}
