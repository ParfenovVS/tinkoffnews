package com.github.parfenovvs.tinkoffnews.ui.news_details;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.parfenovvs.tinkoffnews.R;
import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.ui.MvpLceBaseController;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.RetainingLceViewState;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsDetailsController extends MvpLceBaseController<RecyclerView, NewsDetails, NewsDetailsView, NewsDetailsPresenter>
		implements NewsDetailsView {
	private static final String BUNDLE_NEWS_ID = NewsDetailsController.class.getName() + "news_id";

	@BindView(R.id.toolbar) Toolbar toolbar;
	@BindView(R.id.text) TextView titleTextView;
	@BindView(R.id.date) TextView dateTextView;
	@BindView(R.id.content) TextView contentTextView;

	public static NewsDetailsController newInstance(String newsId) {
		Bundle args = new Bundle();
		args.putString(NewsDetailsController.BUNDLE_NEWS_ID, newsId);
		return new NewsDetailsController(args);
	}

	public NewsDetailsController(Bundle args) {
		super(args);
	}

	@NonNull
	@Override
	protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
		View view = inflater.inflate(R.layout.controller_news_details, container, false);
		ButterKnife.bind(this, view);

		toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
		toolbar.setNavigationOnClickListener(v -> getRouter().handleBack());

		return view;
	}

	@NonNull
	@Override
	public NewsDetailsPresenter createPresenter() {
		return new NewsDetailsPresenter(getArgs().getString(BUNDLE_NEWS_ID));
	}

	@Override
	public NewsDetails getData() {
		return presenter.getData();
	}

	@NonNull
	@Override
	public LceViewState<NewsDetails, NewsDetailsView> createViewState() {
		return new RetainingLceViewState<>();
	}

	@Override
	protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
		Resources resources = getResources();
		if (resources != null) {
			if (pullToRefresh) {
				return getResources().getString(R.string.loading_error);
			} else {
				return getResources().getString(R.string.try_again);
			}
		}
		return "";
	}

	@Override
	public void setData(NewsDetails data) {
		titleTextView.setText(data.getText());
		dateTextView.setText(presenter.formatDate(data.getPublicationDate()));
		contentTextView.setText(data.getContent());
	}

	@Override
	public void loadData(boolean pullToRefresh) {
		presenter.loadData();
	}
}
