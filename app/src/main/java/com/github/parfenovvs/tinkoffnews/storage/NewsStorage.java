package com.github.parfenovvs.tinkoffnews.storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.parfenovvs.tinkoffnews.common.Func1;
import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;

import java.util.List;

public interface NewsStorage {
	@NonNull
	List<NewsItem> getNews();

	@Nullable
	NewsDetails getNewsDetails(String id);

	void transaction(Func1<NewsTransaction> transactionBody);
}
