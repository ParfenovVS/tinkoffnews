package com.github.parfenovvs.tinkoffnews.repository;

import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;

import java.util.List;

import io.reactivex.Observable;

public interface NewsRepository {
	Observable<List<NewsItem>> news(boolean invalidate);

	Observable<NewsDetails> newsDetails(String id, boolean invalidate);
}
