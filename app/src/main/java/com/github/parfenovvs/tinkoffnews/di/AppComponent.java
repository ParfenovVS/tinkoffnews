package com.github.parfenovvs.tinkoffnews.di;

import com.github.parfenovvs.tinkoffnews.repository.DefaultNewsRepository;
import com.github.parfenovvs.tinkoffnews.ui.news_details.NewsDetailsPresenter;
import com.github.parfenovvs.tinkoffnews.ui.news_list.NewsPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {ApiModule.class, NewsModule.class})
@Singleton
public interface AppComponent {
	void inject(DefaultNewsRepository target);

	void inject(NewsPresenter target);

	void inject(NewsDetailsPresenter target);
}
