package com.github.parfenovvs.tinkoffnews.storage;

import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;

import java.util.List;

import io.realm.Realm;

public class NewsRealmTransaction implements NewsTransaction {
	private Realm realm;

	public NewsRealmTransaction(Realm realm) {
		this.realm = realm;
	}

	@Override
	public void clear() {
		realm.deleteAll();
	}

	@Override
	public void add(List<NewsItem> news) {
		realm.copyToRealmOrUpdate(news);
	}

	@Override
	public void add(NewsDetails news) {
		realm.copyToRealmOrUpdate(news);
	}
}
