package com.github.parfenovvs.tinkoffnews.ui.news_list;

import com.github.parfenovvs.tinkoffnews.NewsApplication;
import com.github.parfenovvs.tinkoffnews.common.DateFormatUtil;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;
import com.github.parfenovvs.tinkoffnews.repository.NewsRepository;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NewsPresenter extends MvpBasePresenter<NewsView> {
	private static final Comparator<NewsItem> NEWS_COMPARATOR = (n1, n2) -> -Long.valueOf(n1.getPublicationDate()).compareTo(n2.getPublicationDate());
	private List<NewsItem> news;
	@Inject NewsRepository repository;

	NewsPresenter() {
		NewsApplication.appComponent().inject(this);
	}

	void loadNews(boolean refresh) {
		getView().showLoading(refresh);

		repository.news(refresh)
				.doOnNext(items -> Collections.sort(items, NEWS_COMPARATOR))
				.doOnNext(items -> this.news = items)
				.map(this::toViewModel)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(loaded -> {
					getView().setData(loaded);
					getView().showContent();
				}, throwable -> getView().showError(throwable, refresh));
	}

	void onNewsItemClick(int position) {
		getView().goToNewsDetails(news.get(position).getId());
	}

	private List<NewsViewModel> toViewModel(List<NewsItem> items) {
		List<NewsViewModel> vms = new ArrayList<>();

		for (NewsItem item : items) {
			NewsViewModel vm = new NewsViewModel();

			vm.setText(item.getText());
			vm.setDate(DateFormatUtil.format(item.getPublicationDate()));

			vms.add(vm);
		}

		return vms;
	}
}
