package com.github.parfenovvs.tinkoffnews;

import android.app.Application;

import com.github.parfenovvs.tinkoffnews.di.AppComponent;
import com.github.parfenovvs.tinkoffnews.di.DaggerAppComponent;
import com.github.parfenovvs.tinkoffnews.di.NewsModule;

import io.realm.Realm;

public class NewsApplication extends Application {
	private static AppComponent appComponent;

	public static AppComponent appComponent() {
		return appComponent;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		Realm.init(this);

		buildComponent();
	}

	private void buildComponent() {
		appComponent = DaggerAppComponent.builder()
				.newsModule(new NewsModule())
				.build();
	}
}
