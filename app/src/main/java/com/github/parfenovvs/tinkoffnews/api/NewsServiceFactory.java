package com.github.parfenovvs.tinkoffnews.api;

public interface NewsServiceFactory {
	NewsService createNewsService();
}
