package com.github.parfenovvs.tinkoffnews.ui.news_list;

class NewsViewModel {
	private String text;
	private String date;

	String getText() {
		return text;
	}

	String getDate() {
		return date;
	}

	void setText(String text) {
		this.text = text;
	}

	void setDate(String date) {
		this.date = date;
	}
}
