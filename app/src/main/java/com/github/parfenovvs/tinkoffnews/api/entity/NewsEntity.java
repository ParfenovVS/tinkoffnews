package com.github.parfenovvs.tinkoffnews.api.entity;

public class NewsEntity {
	private String id;
	private String text;
	private DateEntity publicationDate;

	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public DateEntity getPublicationDate() {
		return publicationDate;
	}
}
