package com.github.parfenovvs.tinkoffnews.storage;

import com.github.parfenovvs.tinkoffnews.entity.NewsDetails;
import com.github.parfenovvs.tinkoffnews.entity.NewsItem;

import java.util.List;

public interface NewsTransaction extends RealmTransaction {
	void add(List<NewsItem> news);

	void add(NewsDetails news);
}
