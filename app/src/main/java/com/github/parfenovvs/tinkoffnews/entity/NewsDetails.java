package com.github.parfenovvs.tinkoffnews.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class NewsDetails extends RealmObject {
	@PrimaryKey
	private String id;
	private String text;
	private String content;
	private long publicationDate;
	private long creationDate;
	private long lastModificationDate;

	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public String getContent() {
		return content;
	}

	public long getPublicationDate() {
		return publicationDate;
	}

	public long getCreationDate() {
		return creationDate;
	}

	public long getLastModificationDate() {
		return lastModificationDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setPublicationDate(long publicationDate) {
		this.publicationDate = publicationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	public void setLastModificationDate(long lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}
}
