package com.github.parfenovvs.tinkoffnews.api.entity;

public class NewsDetailsEntity {
	private TitleEntity title;
	private DateEntity creationDate;
	private DateEntity lastModificationDate;
	private String content;

	public TitleEntity getTitle() {
		return title;
	}

	public DateEntity getCreationDate() {
		return creationDate;
	}

	public DateEntity getLastModificationDate() {
		return lastModificationDate;
	}

	public String getContent() {
		return content;
	}

	public void setTitle(TitleEntity title) {
		this.title = title;
	}

	public void setCreationDate(DateEntity creationDate) {
		this.creationDate = creationDate;
	}

	public void setLastModificationDate(DateEntity lastModificationDate) {
		this.lastModificationDate = lastModificationDate;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
