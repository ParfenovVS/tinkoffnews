package com.github.parfenovvs.tinkoffnews.storage;

public interface RealmTransaction {
	void clear();
}
